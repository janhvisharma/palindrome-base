package sheridan;

public class Palindrome {
	
	public static boolean isPalindrome( String input ) {
		
		StringBuilder intermediate = new StringBuilder(input);
        return input.equals(intermediate.reverse().toString());
	}
}
