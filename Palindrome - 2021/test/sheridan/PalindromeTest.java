package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean result = Palindrome.isPalindrome("madam");
		assertTrue("Invalid Palindrome String", result);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean result = Palindrome.isPalindrome("janhvi");
		assertFalse("Invalid Palindrome String", result);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean result = Palindrome.isPalindrome("mom");
		assertTrue("Invalid Palindrome String", result);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean result = Palindrome.isPalindrome("madame");
		assertFalse("Invalid Palindrome String", result);
	}	
}
